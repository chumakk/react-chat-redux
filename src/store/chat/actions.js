import * as types from "./types";

const setMessages = (payload) => ({
  type: types.setMessages,
  payload,
});

const addMessage = (payload) => ({
  type: types.addMessage,
  payload,
});

const setEditMessage = (payload) => ({
  type: types.setEditMessage,
  payload,
});

const deleteMessage = (payload) => ({
  type: types.deleteMessage,
  payload,
});

const toggleLikeMessage = (payload) => ({
  type: types.toggleLikeMessage,
  payload,
});

const setCanScroll = (payload) => ({
  type: types.setCanScroll,
  payload,
});

const updateMessage = (payload) => ({
  type: types.updateMessage,
  payload,
});

const unsetEditMessage = () => ({
  type: types.unsetEditMessage
})

export {
  setMessages,
  addMessage,
  setEditMessage,
  updateMessage,
  deleteMessage,
  toggleLikeMessage,
  setCanScroll,
  unsetEditMessage
};
