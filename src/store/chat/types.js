const setMessages = "chat/set-messages";
const addMessage = "chat/add-message";
const updateMessage = "chat/update-message";
const deleteMessage = "chat/delete-message";
const toggleLikeMessage = "chat/like-message";
const setEditMessage = "chat/set-edit-message";
const unsetEditMessage = "chat/unset-edit-message";
const setCanScroll = "chat/set-can-scroll";

export {
  setMessages,
  addMessage,
  updateMessage,
  deleteMessage,
  toggleLikeMessage,
  setEditMessage,
  unsetEditMessage,
  setCanScroll
};
