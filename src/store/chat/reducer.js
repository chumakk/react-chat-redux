import * as types from "./types";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";

const initialState = {
  chat: {
    ownerId: "owner_id",
    messages: [],
    editMessage: null,
    editModal: false,
    preloader: true,
    canScroll: true,
  },
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.setMessages: {
      return {
        ...state,
        chat: { ...state.chat, messages: action.payload, preloader: false },
      };
    }

    case types.addMessage: {
      const message = {
        id: uuidv4(),
        text: action.payload,
        userId: state.chat.ownerId,
        createdAt: moment.utc().format(),
      };
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: [...state.chat.messages, message],
          canScroll: true,
        },
      };
    }

    case types.updateMessage: {
      const message = { ...state.chat.editMessage, text: action.payload };
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: state.chat.messages.map((mes) =>
            mes.id === message.id ? message : mes
          ),
          editMessage: null,
          editModal: false,
        },
      };
    }

    case types.deleteMessage: {
      const id = action.payload;
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: state.chat.messages.filter((mes) => mes.id !== id),
        },
      };
    }

    case types.toggleLikeMessage: {
      const id = action.payload;
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: state.chat.messages.map((mes) =>
            mes.id !== id ? mes : { ...mes, isLiked: !!!mes.isLiked }
          ),
        },
      };
    }

    case types.setEditMessage: {
      const message = state.chat.messages.find(
        (mes) => mes.id === action.payload
      );
      return {
        ...state,
        chat: { ...state.chat, editMessage: message, editModal: true },
      };
    }

    case types.unsetEditMessage: {
      return {
        ...state,
        chat: { ...state.chat, editMessage: null, editModal: false },
      };
    }

    case types.setCanScroll: {
      return { ...state, chat: { ...state.chat, canScroll: action.payload } };
    }

    default:
      return state;
  }
};

export default rootReducer;
