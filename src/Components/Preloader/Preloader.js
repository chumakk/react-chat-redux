import React from "react";
import "./Preloader.css";

class Preloader extends React.Component {
  render() {
    return (
      <div className="preloader">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
          width="100px"
          height="100px"
          viewBox="0 0 100 100"
          preserveAspectRatio="xMidYMid"
        >
          <circle
            cx="50"
            cy="50"
            fill="none"
            stroke="#6d6d6d"
            strokeWidth="8"
            r="24"
            strokeDasharray="113.09733552923255 39.69911184307752"
          >
            <animateTransform
              attributeName="transform"
              type="rotate"
              repeatCount="indefinite"
              dur="0.8695652173913042s"
              values="0 50 50;360 50 50"
              keyTimes="0;1"
            ></animateTransform>
          </circle>
        </svg>
      </div>
    );
  }
}

export default Preloader;
