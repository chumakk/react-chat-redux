import React from "react";
import { connect } from "react-redux";
import { updateMessage, unsetEditMessage } from "../../store/chat/actions";
import "./Modal.css";

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.message?.text,
    };
    this.input = React.createRef();
    this.onChange = this.onChange.bind(this);
    this.onUpdateMessage = this.onUpdateMessage.bind(this);
  }

  componentDidMount() {
    this.input.current.focus();
    this.input.current.selectionStart = this.state.value.length;
    this.input.current.selectionEnd = this.state.value.length;
  }

  onChange(e) {
    this.setState({
      value: e.target.value,
    });
  }

  onUpdateMessage() {
    if (this.state.value.length === 0) return;
    this.props.updateMessage(this.state.value);
  }

  render() {
    return (
      <div className="edit-modal-wrapper">
        <div className="edit-message-modal modal-shown">
          <div className="modal-header">
            <div className="modal-title">Edit message</div>
            <button
              onClick={this.props.unsetEditMessage}
              className="edit-message-close"
            >
              <svg
                aria-hidden="true"
                className="modal-icon"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 352 512"
              >
                <path
                  fill="#888"
                  d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"
                ></path>
              </svg>
            </button>
          </div>
          <textarea
            ref={this.input}
            className="edit-message-input"
            value={this.state.value}
            onChange={this.onChange}
          ></textarea>
          <button
            onClick={this.onUpdateMessage}
            className="edit-message-button"
          >
            Save
          </button>
        </div>
      </div>
    );
  }
}

const mapState = (state) => ({
  message: state.chat.editMessage,
});

export default connect(mapState, {
  unsetEditMessage,
  updateMessage,
})(Modal);
