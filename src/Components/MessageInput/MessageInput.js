import React from "react";
import { connect } from "react-redux";
import { addMessage } from "../../store/chat/actions";
import "./MessageInput.css";
import icon from "./paper-plane-solid.svg";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textarea: "",
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onSendMessage = this.onSendMessage.bind(this);
  }

  onChangeHandler(e) {
    this.setState({
      textarea: e.target.value,
    });
  }

  onSendMessage() {
    if (this.state.textarea.length === 0) return;
    this.props.addMessage(this.state.textarea);
    this.setState({
      textarea: "",
    });
  }

  render() {
    return (
      <div className="message-input">
        <textarea
          className="message-input-text"
          placeholder="Write message..."
          value={this.state.textarea}
          onChange={this.onChangeHandler}
        ></textarea>
        <div className="button-wrapper">
          <button className="message-input-button" onClick={this.onSendMessage}>
            <img className="sendArrow" src={icon} alt="arrow" />
            Send
          </button>
        </div>
      </div>
    );
  }
}

export default connect(null, {
  addMessage,
})(MessageInput);
