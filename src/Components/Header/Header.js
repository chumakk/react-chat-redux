import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import "./Header.css";

class Header extends React.Component {
  countOfUsers(messages) {
    const uniqueUsers = [];
    messages.forEach((message) => {
      if (!uniqueUsers.includes(message.userId)) {
        uniqueUsers.push(message.userId);
      }
    });
    return uniqueUsers.length;
  }

  dateOfLastMessage(messages) {
    const sortedMessages = [...messages].sort((message, nextMessage) => {
      return new Date(message.createdAt) - new Date(nextMessage.createdAt);
    });
    return sortedMessages[sortedMessages.length - 1]?.createdAt;
  }

  render() {
    const countOfUsers = this.countOfUsers(this.props.messages);
    const countOfMessages = this.props.messages.length;
    const dateOfLastMessage = this.dateOfLastMessage(this.props.messages);
    return (
      <div className="header">
        <div className="header-title">My chat</div>
        <div className="header-users-count-wrapper">
          <span className="header-users-count">{countOfUsers}</span>
          <span> users</span>
        </div>
        <div className="header-messages-count-wrapper">
          <span className="header-messages-count">{countOfMessages}</span>
          <span> messages</span>
        </div>
        <div className="header-last-message-date-wrapper">
          <span>last message at </span>
          <span className="header-last-message-date">
            {dateOfLastMessage && moment(dateOfLastMessage).format("DD.MM.YYYY HH:mm")}
          </span>
        </div>
      </div>
    );
  }
}

const mapState = (state) => ({
    messages: state.chat.messages
})

export default connect(mapState, null)(Header);
