import React from "react";
import { connect } from "react-redux";
import { setMessages, setEditMessage } from "../store/chat/actions";
import "./Chat.css";
import Header from "./Header/Header";
import MessageList from "./MessageList/MessageList";
import MessageInput from "./MessageInput/MessageInput";
import Preloader from "./Preloader/Preloader";
import Modal from "./Modal/Modal";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  componentDidMount() {
    fetch(this.props.url)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.props.setMessages(data);
      });
    document.addEventListener("keydown", this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  handleKeyDown(e) {
    if (e.key === "ArrowUp" && !this.props.editModal) {
      const lastMessage = [...this.props.messages]
        .reverse()
        .find((mes) => mes.userId === this.props.ownerId);
      if (lastMessage) {
        e.preventDefault();
        this.props.setEditMessage(lastMessage.id);
      }
    }
  }

  render() {
    return (
      <div className="chat">
        {!this.props.preloader ? (
          <div className="chat-wrapper">
            <Header />
            <MessageList />
            <MessageInput />
            {this.props.editModal && <Modal />}
          </div>
        ) : (
          <Preloader />
        )}
      </div>
    );
  }
}

const mapState = (state) => ({
  preloader: state.chat.preloader,
  ownerId: state.chat.ownerId,
  messages: state.chat.messages,
  editModal: state.chat.editModal,
});

export default connect(mapState, {
  setMessages,
  setEditMessage,
})(Chat);
